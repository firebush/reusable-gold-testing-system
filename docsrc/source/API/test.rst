
Test
====

Defines a given test and all related logic.
There is one Test object per test file.
A given test can contain many different steps refereed to a TestRuns.
Some of the member of the Test object may also be accessed globally
without reference to the Test object, such as the Setup member.

.. autoclass:: autest.core.test.Test()
    :members:
    :inherited-members:


