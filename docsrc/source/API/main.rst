
User API
########

The API consists of objects and functions that can be used to define a test.

.. toctree::
    :maxdepth: 1

    test.rst
    testrun.rst
    process.rst
    streams.rst
    setup.rst
    processes.rst
    disk.rst
    directory.rst
    file.rst
    conditions.rst
    when.rst
    testers.rst

